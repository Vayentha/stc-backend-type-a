import iexfinance
import mysql.connector as mysql
from iexfinance import get_stats_intraday
from iexfinance import get_iex_corporate_actions
from iexfinance.stocks import Stock
import json
from pandas import DataFrame


"""


    if you call stock.get_financials(),
    output should look like the following:
    
    (an example nvidia stock)
    {
    "('NVDA', '2020-04-30')": {
        "cashChange": 4598000000,
        "cashFlow": 909000000,
        "costOfRevenue": 1076000000,
        "currency": "USD",
        "currentAssets": 19584000000,
        "currentCash": 15494000000,
        "currentDebt": 100000000,
        "fiscalDate": "2020-04-26",
        "grossProfit": 2004000000,
        "longTermDebt": 7478000000,
        "netIncome": 917000000,
        "operatingExpense": 2104000000,
        "operatingIncome": 976000000,
        "operatingRevenue": 3080000000,
        "reportDate": "2020-04-30",
        "researchAndDevelopment": 735000000,
        "shareholderEquity": 13099000000,
        "shortTermDebt": 100000000,
        "totalAssets": 23254000000,
        "totalCash": 16354000000,
        "totalDebt": 7578000000,
        "totalLiabilities": 10155000000,
        "totalRevenue": 3080000000
    }
}


"""

dbc = mysql.connect(host=host, database=db, user=msuser, password=passw)

# three important functions


# fetch all from a table. change the execute line to any sql you want
def fetch_all_in_table(tb):
    csr = dbc.cursor()  #dbc in this case is any mysql database connection
    try:
        csr.execute(f"SELECT * FROM {tb}")
        res = csr.fetchall()
        for item in res:
            print(item)
    except Exception as e:
        print(e)


def insertIEX(tic, cchange, cflow, cor):
    csr = dbc.cursor()  #database connection
    csr.execute(f''' INSERT INTO iexdata(ticker, cashChange, cashFlow, costOfRevenue) VALUES ('{tic}', {cchange}, {cflow}, {cor})''')
    dbc.commit()


# find the number associated with a tag (i.e cashChange, costOfRevenue)
# from an IEXfinance blob.
def parse_iex_json(jsobj, findKey):
    for item in jsobj.keys():
        print(f"primary key is {item}")
        for key, val in jsobj[item].items():
            if key == findKey:
                return val



# this func will get stock data from iexfinance and will
# insert it into the database (mysql.)
def get_stock_and_insert_financials(stock_name):
    tck1 = Stock(stock_name, token=iextoken, output_format='pandas')
    temp_frame = tck1.get_financials()
    temp_json = DataFrame.to_json(temp_frame)
    ugly_json = json.loads(temp_json)   # use this for finding values
    pretty_json = json.dumps(ugly_json, indent=4)
    print(pretty_json)
    tcashflow = parse_iex_json(ugly_json, "cashFlow")
    tcashChange = parse_iex_json(ugly_json, "cashChange")
    tcor = parse_iex_json(ugly_json, "costOfRevenue")
    print(tcashflow)
    print(tcashChange)
    print(tcor)
    print("inserting into iexdb")
    insertIEX(stock_name, tcashChange, tcashflow, tcor)
    print("check db now")

