from wsgiref.simple_server import make_server
from pyramid.view import view_config
from pyramid.config import Configurator
from pyramid.response import Response
from pyramid.renderers import render_to_response
from pyramid.request import Request

import mysql.connector as mysql
from stonkwebsite.stockmanager import serveStocks as ss


dbc = mysql.connect(host=ss.host, database=ss.db, user=ss.msuser, password=ss.passw)
if dbc:
    print("mysql connected!")

'''
    This is the website API file. Pretty much every single API call will
    have its associated code here. 
'''


# ============== API CALLS HERE ===============

# make this into a list of users from the database
def list_all_users(request):
    return Response('List of users\n')


# get an individual user from the database
def get_user(request):
    qst = request.path_qs   # get the actual query from the user
    parsed_qst = qst.replace('/users/', '')
    breakstr = parsed_qst.split('_')
    print(breakstr)   #this should have our firstname_lastname combo
    if ss.check_user_in_db_name(breakstr[0], breakstr[1]) == 1:
        return Response('A user named %(name1)s %(name2)s exists in the database\n' % request.matchdict)
    else:
        return Response('no user by the name of %(name1)s %(name2)s exists in the database.\n' %request.matchdict)


# TODO: make this work
def delete_user_from_db(request, userFN, userLN):
    return


# TODO: make this work
def enter_user_db(request, userFN, userLN):
    return


# just an endpoint to the fetch/ subdirectory, does nothing right now
def api_user_stocks(request):
    return Response('user stocks endpoint\n')


# API call to get the user stocks
# looks up by USERNAME, not fname/lastname
def get_user_owned_stocks(request):
    qst = request.path_qs
    # print(qst)
    parsed_qst = qst.replace('/fetch/', '')
    # breakstr = parsed_qst.split('_')
    slist = ss.get_user_stocks(parsed_qst)
    for item in slist:
        for subitem in item:
            r = subitem
    return Response(f'{r}\n')


def api_add_db_stocks(request):
    return

def api_get_stock_amounts(request):
    qst = request.path_qs
    parsed_qst = qst.replace('/amounts/', '')
    print(parsed_qst)
    current_uid = ss.get_userID_from_username(parsed_qst)
    print(current_uid)
    for item in current_uid:
        for sub in item:
            actual_uid = sub
    print(actual_uid)
    owned = ss.build_stock_dict(actual_uid)
    owned = str(owned).replace('\'', '\"')
    return Response(f'{{ \"stonks\":   {owned}}}')

@view_config(route_name='bigpost', renderer='json', request_method='POST')
def api_post_test(request):
    jobj = request.POST
    print(jobj)
    finaldict = []
    for item in jobj.items():
        finaldict.append(item)
    return {'POST': finaldict}


@view_config(route_name='creds', renderer='json', request_method='POST')
def post_creds(request):
    req2 = request.POST.get("uname")
    token = request.POST.get("token")
    if req2 is None:
        return {'Error': 'nonetype set'}
    print(req2)
    print(token)




# ======= END API CALLS =======


if __name__ == '__main__':
    config = Configurator()

    config.add_route('users', '/users')
    config.add_route('user', '/users/{name1}_{name2}')
    config.add_route('fetch', '/fetch')
    config.add_route('fetchlist', '/fetch/{name1}')
    config.add_route('add', '/add/{sname}')
    config.add_route('amounts', '/amounts/{sname}')
    config.add_route('bigpost', '/bigpost/')
    config.add_route('creds', '/creds/')


    config.add_view(list_all_users, route_name='users')
    config.add_view(get_user, route_name='user')
    config.add_view(api_user_stocks, route_name='fetch')
    config.add_view(get_user_owned_stocks, route_name='fetchlist')
    config.add_view(api_add_db_stocks, route_name='add')
    config.add_view(api_get_stock_amounts, route_name='amounts')
    config.add_view(api_post_test, route_name='bigpost')
    config.add_view(post_creds, route_name='creds')
    config.scan()
    app = config.make_wsgi_app()
    server = make_server('127.0.0.1', 6543, app)
    server.serve_forever()

# ===== EDIT LOG =====

# [6-20-2020 -- SAYAN] created API for fetching both users and stocks