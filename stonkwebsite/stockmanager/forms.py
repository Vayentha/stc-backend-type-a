from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class SignUpForm(UserCreationForm):
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')
    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2', )

class AddTickerForm(forms.Form):
    ticker = forms.CharField(label='Stock ticker', max_length=100)
    num_shares = forms.IntegerField(label="Number of shares")

class DeleteTickerForm(forms.Form):
    ticker = forms.CharField(label='Stock ticker', max_length=100)

class UpdateTickerForm(forms.Form):
    ticker = forms.CharField(label='Stock ticker', max_length=100)
    num_shares = forms.IntegerField(label="New number of shares")