from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login as auth_login
# Create your views here.

from .forms import SignUpForm, AddTickerForm, DeleteTickerForm, UpdateTickerForm
from . import serveStocks as ss
import itertools

def add_stock(request):
    if request.method == 'POST':
        form = TickerForm(request.POST)
        if form.is_valid():
            return "success"
    else:
        form = TickerForm()
    
    return render(request, 'stockmanager/home.html', {})

@login_required
def home(request):
    user = request.user
    userID = ss.get_userID_from_username(user)[0][0]
    
    update_message = "Make changes to your portfolio here."
    if request.method == 'POST' and 'add_form' in request.POST:
        add_form = AddTickerForm(request.POST)
        if add_form.is_valid():
            ss.insert_ticker_and_shares(userID, add_form.cleaned_data["ticker"], add_form.cleaned_data["num_shares"])
            update_message = "Ticker added to user."
    if request.method == 'POST' and 'delete_form' in request.POST:
        del_form = DeleteTickerForm(request.POST)
        if del_form.is_valid():
            ss.delete_ticker_and_shares(userID, del_form.cleaned_data["ticker"])
            update_message = "Ticker removed from user."
    if request.method == 'POST' and 'update_form' in request.POST:
        update_form = UpdateTickerForm(request.POST)
        if update_form.is_valid():
            ss.update_ticker_and_shares(userID, update_form.cleaned_data["ticker"], update_form.cleaned_data["num_shares"])
            update_message = "Ticker updated."

    add_form = AddTickerForm()
    del_form = DeleteTickerForm()
    update_form = UpdateTickerForm()

    portfolio_list = ss.get_stocks_owned_as_json(userID)
    portfolio_val = ss.calculate_portfolio_value(userID)
    stockinfo_list = ss.getinfo_aboutuserstocks(userID)
    stocks = ss.build_stock_dict(userID)

    context = {   # pass context variables here. Can pass as many as needed to make a nice dynamic webpage
        'stocks': stocks,
        'user': user,
        'add_form': add_form,
        'delete_form': del_form,
        'update_form': update_form,
        'portfolio': portfolio_val,
        'update_message': update_message,
        'infolist': stockinfo_list,
    }
    # ss.get_stock_and_insert_financials('AAPL', "i")
    return render(request, 'stockmanager/home.html', context)

def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            auth_login(request, user)
            return redirect('home')
    else:
        form = SignUpForm()
    return render(request, "registration/signup.html", {'form': form})

@login_required
def leaderboard(request):
    return render(request, "leaderboard.html")

@login_required
def recommended(request):
    user = request.user
    userID = ss.get_userID_from_username(user)[0][0]
    bySecurityScore = ss.recommendbasedon("SecurityScore DESC")
    byMostExpensive = ss.recommendbasedon("price DESC")
    byLeastExpensive = ss.recommendbasedon("price ASC")
    recommendedIndustry = ss.recommended_stocks_byindustry(userID)
    context = {
        'recommendedtable': recommendedIndustry,
        'securitytable': bySecurityScore,
        'expensivetable': byMostExpensive,
        'cheaptable': byLeastExpensive,
    }
    return render(request, "recommended.html", context)

#======================= EDIT LOG =================
# [6-14-2020 -- SAYAN] removed unnecessary tuple code from line 9