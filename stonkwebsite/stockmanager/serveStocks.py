import mysql.connector as mysql     #connect to local mysql
# IEXFINANCE imports below
from iexfinance.stocks import Stock
import json                     # parse json library
from pandas import DataFrame    # allows IEX dataframes to be turned into jsons and vice versa
import yaml


# ========= ALL THE SETTINGS AND STUFF =========== #

# gets SQL data from yaml file(config folder) -- CHANGE THIS TO WHATEVER YOUR FILE CONFIG IS
def getData(item):
    with open(r'/Users/vayentha/cs411-project-stocks/stonkwebsite/stockmanager/config/dbconfig.yaml') as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
        return data[item]

# mysql data here
host = f"{getData('ip')}"
db = f"{getData('server')}"
msuser = f"{getData('user')}"
passw = f"{getData('password')}"

iextoken = "pk_6c06024a27b84bc4a2c00ffbebe220c4"
dbc = mysql.connect(host=host, database=db, user=msuser, password=passw) # db connection here

# ========= GETTING THE DATA BASED ON THE USER ID =========== #

# getting the userID given a userName
def get_userID_from_username(userName):
    userID = 0
    user_query = (f"SELECT userID FROM users WHERE username='{userName}'")
    csr = dbc.cursor()
    csr.execute(user_query)
    dbres = csr.fetchall()
    if dbres:
        return dbres
    return userID

# getting the stocks owned based on the userID
def get_stocks_owned(userID):
    csr = dbc.cursor()
    dbres = ""
    try:
        id = str(userID)
        select_query = (
            f"SELECT * "
            f"FROM stocksOwned "
            f"WHERE userID={id}"
        )
        print(select_query)
        csr.execute(select_query)
        dbres = csr.fetchall()
        return dbres
    except Exception as e:
        print(e)

# ========= INSERTING, UPDATING AND DELETING DATA IN THE STOCKS OWNED TABLE =========== #
def get_existing_shares(userID, ticker):
    num_shares = 0
    user_query = (f"SELECT numShares FROM stocksOwned WHERE userID='{userID}' AND ticker='{ticker}'")
    csr = dbc.cursor()
    csr.execute(user_query)
    dbres = csr.fetchall()
    if dbres:
        return dbres[0][0]
    return num_shares

# given a userID, inserts ticker, num_shares into stocksOwned table
def insert_ticker_and_shares(userID, ticker, num_shares):
    num_existing_shares = get_existing_shares(userID, ticker)
    print(ticker, num_shares, num_existing_shares)
    if num_existing_shares != 0:
        update_ticker_and_shares(userID, ticker, num_shares)
        return
    insert_query = (
        f"""INSERT INTO stocksOwned 
          VALUES ({userID}, '{ticker}', {num_shares}) """)
    csr = dbc.cursor()
    csr.execute(insert_query)
    dbc.commit()

# given a userID, deletes ticker, num_shares into stocksOwned table
def delete_ticker_and_shares(userID, ticker):
    delete_query = (
        f"DELETE FROM stocksOwned "
        f"WHERE userID={userID} AND ticker='{ticker}'")
    csr = dbc.cursor()
    csr.execute(delete_query)
    dbc.commit()


# given a userID, ticker, and number of shares,
# attempts to overwrite old number of shares with new quantity
def update_ticker_and_shares(userID, ticker, num_shares):
    update_query = (
        f"UPDATE stocksOwned "
        f"SET numShares= numShares + {num_shares} "
        f"WHERE userID={userID} "
        f"AND ticker='{ticker}'"
    )
    csr = dbc.cursor()
    csr.execute(update_query)
    dbc.commit()

# ========= CALCULATING THE TOTAL PORTOLIO SCORE BASED ON THE TABLES IEXDATA AND STOCKSOWNED =========== #

# given a userID,
# find and sum up current portfolio value
def calculate_portfolio_value(userID):
    portfolio_val = 0.0
    portfolio_query = (f"SELECT ROUND(SUM(S.numShares * I.price), 2) FROM iexdata I JOIN stocksOwned S ON I.ticker=S.ticker WHERE userID={userID} GROUP BY S.userID")
    csr = dbc.cursor()
    csr.execute(portfolio_query)
    dbres = csr.fetchall()
    if dbres:
        return dbres[0][0]
    return portfolio_val

# ========= RETURNS THE USER'S MORE POPULAR INDUSTRY BASED ON HIGHEST NUMBER OF STOCK =========== #

# given a userID,
# find the most popular industry based on highest stock
def recommended_stocks_byindustry(userID):
    recommendedtable = []
    csr = dbc.cursor()
    try:
        csr.execute(f"""
            SELECT d.ticker, d.price, d.industry, s.SecurityScore
            FROM (SELECT i.industry, SUM(s.numShares) as sharesCount
                FROM stocksOwned s LEFT JOIN iexdata i ON (s.ticker = i.ticker)
                WHERE s.userID = '{userID}'
                GROUP BY i.industry
                HAVING sharesCount = (SELECT MAX(sharesCount) FROM (SELECT SUM(s.numShares) as sharesCount
                                    FROM stocksOwned s LEFT JOIN iexdata i ON (s.ticker = i.ticker)
                                    WHERE s.userID = '{userID}'
                                    GROUP BY i.industry) temp)) as temp, 
                iexdata d JOIN stocks s ON (d.ticker = s.ticker)
            WHERE temp.industry = d.industry AND s.ticker NOT IN (SELECT ticker FROM stocksOwned WHERE userID = '{userID}')
            ORDER BY s.SecurityScore DESC
            LIMIT 4
            """)
        res = csr.fetchall()
        for item in res:
            recommendedtable.append(item)
    except Exception as e:
        print(e)
    print(recommendedtable)
    return recommendedtable

# ========= FINDS THE TOP 5 SECURITY SCORE STOCKS =========== #

def recommendbasedon(toOrder):
    dbres = []
    csr = dbc.cursor()
    try:
        csr.execute(f"SELECT s.CompanyName, s.Ticker, i.price, i.Industry, s.SecurityScore FROM stocks s JOIN iexdata i ON (s.ticker = i.ticker) ORDER BY {toOrder} LIMIT 4")
        res = csr.fetchall()
        for item in res:
            dbres.append(item)
    except Exception as e:
        print(e)
    print(dbres)
    return dbres

# ========= GETS VALUES FROM TABLES ABOUT USER STOCKS TO POPULATE DROPDOWN =========== #

# TODO: fix this function
def getinfo_aboutuserstocks(userID):
    stockinfo = []
    csr = dbc.cursor()
    try:
        csr.execute(f"SELECT s.ticker, s.numShares, i.industry, st.SecurityScore, i.cashFlow, i.costOfRevenue FROM stocksOwned s LEFT JOIN iexdata i ON (i.ticker = s.ticker) LEFT JOIN stocks st ON (st.ticker = s.ticker) WHERE s.userID = '{userID}' ORDER BY s.numShares DESC")
        res = csr.fetchall()
        for item in res:
            stockinfo.append(item)
    except Exception as e:
        print(e)
    print(f'stockinfo is {stockinfo}')
    return stockinfo
    
# ========= GETTING CURRENT DATA FOR USER STOCKS ========= #
def get_stocks_owned_as_json(userID):
    stock_list = get_stocks_owned(userID)
    stock_json = []
    for stock in stock_list:
        stock_json.append(
            stock[1],
        )
    print(stock_json)
    return stock_json

# gets a list of all stocks in the stock_list above and builds a dictionary with their
# company name, ticker, and current price. called in views.py
# to display current information.
def build_stock_dict(userID):
    final_stock_dict = []
    st = get_stocks_owned_as_json(userID)
    # print(st)
    for i in range(0, len(st)):
        dict_item = {
            'stockName': f'{get_company_name(st[i])}',
            'stockTicker': f'{st[i]}',
            'stockPrice': f'{get_stock_price(st[i])}'
        }
        # print(dict_item)
        final_stock_dict.append(dict_item)
    # print(final_stock_dict)
    return final_stock_dict

# ========= INSERTING DATA AND UPDATING DATA IN THE IEXDATA TABLE =========== #

# gets the company name
def get_company_name(tic):
    stc = Stock(tic, token=iextoken, output_format='pandas')
    preparse = stc.get_company()
    # print(preparse)
    jtemp = DataFrame.to_json(preparse)
    second = json.loads(jtemp)
    companyName = parse_iex_json(second, "companyName")
    return companyName

# gets the sector name
def get_sector_name(tic):
    stc = Stock(tic, token=iextoken, output_format='pandas')
    preparse = stc.get_company()
    jtemp = DataFrame.to_json(preparse)
    second = json.loads(jtemp)
    sector = parse_iex_json(second, "sector")
    print(sector)
    return sector

# insert code into IEX database (works for remote mysql, use insert_local() to insert into local sqlite3 db
def insertIEX(tic, price, cchange, cflow, cor, date1, sector):
    csr = dbc.cursor()  #database connection
    csr.execute(f''' INSERT INTO iexdata(ticker, price, cashChange, cashFlow, costOfRevenue, reportDate, industry) VALUES ('{tic}', '{price}', '{cchange}', '{cflow}', '{cor}', '{date1}', '{sector}')''')
    dbc.commit()


# updates existing information in database for fresh data each time
def updateIEX(tic, price, cchange, cflow, cor, date1, sector):
    csr = dbc.cursor()
    csr.execute(f"""
    UPDATE iexdata SET
    price = '{price}',
    cashChange = '{cchange}',
    cashFlow = '{cflow}',
    costOFRevenue = '{cor}',
    reportDate = '{date1}',
    industry = '{sector}'
    WHERE ticker = '{tic}'
    """)
    dbc.commit()

# find the number associated with a tag (i.e cashChange, costOfRevenue)
# from an IEXfinance blob.
def parse_iex_json(jsobj, findKey):
    for item in jsobj.keys():
        # print(f"primary key is {item}")
        for key, val in jsobj[item].items():
            if key == findKey:
                return val

# this func will get stock data from iexfinance and will
# insert it into the database (mysql.)
def get_stock_and_insert_financials(stock_name, udt):
    try:
        tck1 = Stock(stock_name, token=iextoken, output_format='pandas')
        temp_frame = tck1.get_financials()
        temp_json = DataFrame.to_json(temp_frame)
        ugly_json = json.loads(temp_json)   # use this for finding values
        pretty_json = json.dumps(ugly_json, indent=4)
        print(pretty_json)
        tprice = get_stock_price(stock_name)
        tcashflow = parse_iex_json(ugly_json, "cashFlow")
        tcashChange = parse_iex_json(ugly_json, "cashChange")
        tcor = parse_iex_json(ugly_json, "costOfRevenue")
        tdate = parse_iex_json(ugly_json, "reportDate")
        tindustry = get_sector_name(stock_name)
        print(tprice)
        print(tcashflow)
        print(tcashChange)
        print(tcor)
        print(tdate)
        print(tindustry)
        if udt == 'i':
            print("inserting into iexdb")
            insertIEX(stock_name, tprice, tcashChange, tcashflow, tcor, tdate, tindustry) # magic happens here
        if udt == 'u':
            print("updating iexdb")
            updateIEX(stock_name, tprice, tcashChange, tcashflow, tcor, tdate, tindustry)
        print("check db now")
    except Exception as e:
        print(e)
        print("error inserting into DB")


# ========= OTHER FUNCTIONS THAT I DON'T KNOW WHAT THEY ARE ========= #

# gets all users (by firstname and lastname) who own a stock ticker
# advanced SQL functionality
def get_all_users_who_own_stock(tic):
    csr = dbc.cursor()
    try:
        csr.execute(f"""
            SELECT firstName, lastName, stocksOwned
            FROM users WHERE
            stocksOwned like '%{tic}%'
            GROUP BY firstName, lastName, stocksOwned
        """)
        res = csr.fetchall()
        for item in res:
            print(item)
    except Exception as e:
        print(e)


# insert user into a database: works as of 6-22-2020
# IMPORTANT: make sure auto-increment is on in your SQL db for the UserID
def insert_user(userFN, userLN):
    if check_user_in_db_name(userFN, userLN) == 1:
        return
    else:
        try:
            fresh_stock_json = "{\"stocksOwned\": []}"
            csr = dbc.cursor()
            csr.execute(f"""
                INSERT INTO users(firstName, lastName, stocksOwned)
                VALUES ('{userFN}', '{userLN}', '{fresh_stock_json}')
                """)
            dbc.commit()
        except Exception as e:
            print(e)

"""
    gets the current stock price from a json- just the price.
    
    takes the ticker (all caps) and 
"""
def get_stock_price(ticker):
    final_dict = {}
    stc = Stock(ticker, token=iextoken, output_format='pandas')
    parse = DataFrame.to_json(stc.get_price())
    pretty = json.loads(parse)
    final_dict.update(pretty[ticker])
    # print(final_dict[ticker])  # should contain stock price
    return final_dict[ticker]


# ==================== TODO: REFACTOR

def check_user_in_db_name(userFN, userLN):
    csr = dbc.cursor()
    dbres = ""
    try:
        csr.execute(f"SELECT EXISTS(SELECT firstName, lastName from users WHERE users.firstName = '{userFN}' and users.lastName = '{userLN}')")
        dbres = csr.fetchall()
        # print(dbres)
        for item in dbres:
            # print(item)
            print(item[0])
            return item[0]  # return 0 or 1 here
    except Exception as e:
        print(e)


def check_username_in_db(un):
    csr = dbc.cursor()
    dbres = ""
    try:
        csr.execute(f"SELECT EXISTS(SELECT username from users WHERE users.username = '{un}')")
        dbres = csr.fetchall()
        # print(dbres)
        for item in dbres:
            # print(item)
            print(item[0])
            return item[0]  # return 0 or 1 here
    except Exception as e:
        print(e)


def get_user_stocks(userName):
    csr = dbc.cursor()
    dbres = ""
    try:
        # csr.execute(f"SELECT stocksOwned from users WHERE users.firstName = '{userFN}' and users.lastName = '{userLN}'")
        csr.execute(f"SELECT stocksOwned from users WHERE users.username ='{userName}'")
        dbres = csr.fetchall()
        print(dbres)
        return dbres
    except Exception as e:
        print(e)


def get_user_stock_json(userN):
    xv = get_user_stocks(userN)
    for item in xv:
        for subitem in item:
            print(subitem)
            return subitem


def update_user(userN, ust):
    csr = dbc.cursor()   # sql connection
    # print(f" ust is {ust}")
    csr.execute(f"""
                UPDATE users SET stocksOwned = '{ust}'
                WHERE username = '{userN}'
                """)
    dbc.commit()
    print(f"updated {userN}")

# add a stock to the user's json. checks if stock ticker is valid
# and also checks if that ticker is in the user's stock list already.
def add_user_stock(un, tic):
    if check_username_in_db(un) == 0: # must work with just username
        print("error: no such user in database")
    else:
        jsc = get_user_stock_json(un)
        good_json = json.loads(jsc)
        print(json.dumps(good_json, indent=4))
        try:
            checkstc = Stock(tic, token=iextoken, output_format='pandas')
            # print(checkstc.get_financials())
        except Exception as e:
            print(e)
            return
        print(good_json['stocksOwned'])
        if tic in good_json['stocksOwned']:
            print("ticker already exists in user json, returning")
            return
        good_json["stocksOwned"].append(tic)
        print(good_json)
        x =  str(good_json).replace("'", "\"")
        print(f" x = \"{x}\"")
        update_user(un, x)


# TODO: add this
def stock_search_global(tic):
    if tic is None:
        return


# TODO: add this
def calculate_security_score():
    return
#==================================================

#gets n (default 10) top performing stocks by security score
#returns list of n top performing stocks in desc order.
def get_top_performing_stocks(n=10):
    try:
        csr = dbc.cursor()
        csr.execute(f"""
                SELECT Ticker, SecurityScore
                FROM stocks
                ORDER BY SecurityScore DESC
                LIMIT {n} 
                """)
        res = csr.fetchall()
        return res
    except Exception as e:
        print(e)




#======================= EDIT LOG =================
# [6-14-2020 -- SAYAN] Added functions to take stocks from remote user table
# [6-15-2020 -- SAYAN] Added config yaml file to store SQL user info
# [6-15-2020 -- SAYAN] Added function to update existing database info, works without hassle
# [6-19-2020 -- SAYAN] Made it possible to supply a list of user stocks to the build_stock_dict
# function and generate a view on the webpage from that list.
# [6-19-2020 -- SAYAN] made function parse_user_stocks_from_db to get the list of user stocks from the database
# [6-22-2020 -- SAYAN] added a function to put users into the users DB
# [6-22-2020 -- SAYAN] added a function to check which users own a specific stock
